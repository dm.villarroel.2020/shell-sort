#|/usr/bin/env python3

songs = {'quedate': 3000000 , 'mala mujer': 1500000}
import sys

def sort_music(songs : list) -> list:
    return sorted(songs, key=lambda x: x[1], reverse=True)


def main():
    args = sys.argv[1:]
    
    if len(args) % 2 != 0:
        print("Error: Debes proporcionar pares de canción y número de reproducciones.")
        return

    song_data : list = []
    for i in range(0, len(args), 2):
        song_name = args[i]
        num_reproducciones = int(args[i + 1])
        song_data.append((song_name, num_reproducciones))

    song_dict : dict = dict(song_data)

    sorted_songs : list = list(song_dict.items())
    sort_music(sorted_songs)

    sorted_dict : dict = dict(sorted_songs)
    print(sorted_dict)

if __name__ == '__main__':
    main()
